#+TITLE: Merge Sort Experiment
#+AUTHOR: VLEAD
#+DATE: [2018-05-04 Fri]
#+TAGS: pmbl lu task txta
#+EXCLUDE_TAGS:
#+OPTIONS: ^:nil' prop:t
#+SETUPFILE: ../org-templates/level-1.org

* Experiment Id                                                       :expId:
:PROPERTIES:
:SCAT: expId
:CUSTOM_ID: merge-sort
:END:

* Type of Document                                              :docType:
:PROPERTIES:
:SCAT: docType
:CUSTOM_ID: A1
:END:

* Content of Merge Sort                                               :title:
:PROPERTIES:
:SCAT: title
:CUSTOM_ID: merge-sort-cnt-title
:END:

* Preamble                                                             :pmbl:
:PROPERTIES:
:SCAT: pmbl
:CUSTOM_ID: merge-sort-exp
:END:

* Merge Sort Unit                                                        :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: merge-sort-unit
:END:

** Merge Sort Introduction                                             :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: merge-sort-intro
:END:

*** Merge Sort Involves the following steps                            :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: merge-sort-steps
:END:
Split - Splits the input array in two halves, 
Merge Sort - Perform mergesort on the two halves, 
Merge - Merge the two halves to get the sorted array

*** Picture of Merge sort                                              :reqa:
:PROPERTIES:
:CUSTOM_ID: merge-sort-intro-pic
:SCAT: reqa
:TYPE: Image
:END:
The image is a flowchart of the mergesort that is
illustrated with an input array at the top that is split;
mergesort applied on the split arrays; and the split arrays
merged in the downstream to produce a sorted array.

*** Video on Merge Sort                                                :vida:
:PROPERTIES:
:CUSTOM_ID: merge-sort-intro-video
:SCAT: vida
:CAPTION: Merge sort algorithm
:URL: https://youtu.be/TzeBrDU-JaY
:TIME-SLICE: {"start_time": 10.21, "end_time": 15:22} 
:END:

*** Image of Merge Sort                                                  :imga:
:PROPERTIES:
:CUSTOM_ID: merge-sort-intro-img
:SCAT: imga
:URL: https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Merge_sort_algorithm_diagram.svg/450px-Merge_sort_algorithm_diagram.svg.png
:CAPTION: Merge Sort Cirle
:END:

* Split									 :lu:
:PROPERTIES:
:CUSTOM_ID: split-illustrate-lu
:SCAT: lu
:END:
** Illustrate Split                                                    :task:
:PROPERTIES:
:CUSTOM_ID: split-illustrate-task
:SCAT: task
:END:

*** Split even number of elements                                      :txta:
:PROPERTIES:
:CUSTOM_ID: split-even
:SCAT: txta
:END:
When an an array with even number of elements is split, the
output is two arrays with equal number of elements. 

*** Show the even split                                                :reqa:
:PROPERTIES:
:CUSTOM_ID: split-even-show
:SCAT: reqa
:TYPE: Image
:END:
Provide a diagram where an input array of 8 elements is
split into two arrays of 4 contiguous elements. 

*** Split odd number of elements                                       :txta:
:PROPERTIES:
:CUSTOM_ID: split-odd
:SCAT: txta
:END:
When an an array with odd number of elements is split, the
output is two arrays with one array containing one element
greater than the other.

*** Show the odd split                                                 :reqa:
:PROPERTIES:
:CUSTOM_ID: split-odd-show
:SCAT: reqa
:TYPE: Image
:END:
Provide a diagram where an input array of 7 elements is
split into two arrays of 4 and 3 contiguous elements. 

* Perform Split                                                          :lu:
:PROPERTIES:
:CUSTOM_ID: split-exercise-lu
:SCAT: lu
:END:

** Split Exercise                                                      :task:
:PROPERTIES:
:CUSTOM_ID: split-exercise-task
:SCAT: task
:END:

*** Split on even array                                                :txta:
:PROPERTIES:
:CUSTOM_ID: split-exercise-even
:SCAT: txta
:END:
An even array is provided, perform the split into two equal
halves.

*** Perform the split                                                  :reqa:
:PROPERTIES:
:CUSTOM_ID: split-even-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs
the split on an array containing even number of elements.

*** Split on an odd array                                              :txta:
:PROPERTIES:
:CUSTOM_ID: split-exercise-odd
:SCAT: txta
:END:
An array with odd number of elements is provided, perform
the split into two parts where one of the arrays has one
element more than the other.

*** Perform the split                                                  :reqa:
:PROPERTIES:
:CUSTOM_ID: split-odd-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an artifact where a student can interactively
perform the split on an array containing odd number of
elements.

* Merge                                                                  :lu:
:PROPERTIES:
:CUSTOM_ID: merge-illustrate-lu
:SCAT: lu
:END:

** Illustrate Merge                                                    :task:
:PROPERTIES:
:CUSTOM_ID: merge-illustrate-task
:SCAT: task
:END:

*** Merge elements                                                     :txta:
:PROPERTIES:
:CUSTOM_ID: merge-describe
:SCAT: txta
:END:
Given two arrays of numbers, they are merged to produce a
single array that is sorted where the first element in the
merged array is the least and the last element in the array
is the highest. 

*** Show the merge                                                     :reqa:
:PROPERTIES:
:CUSTOM_ID: merge-show
:SCAT: reqa
:TYPE: Image
:END:
Provide a diagram where two input arrays are merged
producing an array that is sorted. 

* Perform Merge                                                          :lu:
:PROPERTIES:
:CUSTOM_ID: merge-exercise-lu
:SCAT: lu
:END:

** Merge Exercise                                                      :task:
:PROPERTIES:
:CUSTOM_ID: merge-exercise-task
:SCAT: task
:END:

*** Merge two arrays                                                   :txta:
:PROPERTIES:
:CUSTOM_ID: merge-exercise
:SCAT: txta
:END:
Two arrays of integers are provided, perform the merge to
produce a single array that is sorted.

*** Perform the Merge                                                  :reqa:
:PROPERTIES:
:CUSTOM_ID: merge-even-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs
the merge on an array containing even number of elements.

*** Perform the Merge                                                  :reqa:
:PROPERTIES:
:CUSTOM_ID: merge-odd-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs
the merge on an array containing odd number of elements.

* Merge Sort                                                             :lu:
:PROPERTIES:
:CUSTOM_ID: mergesort-illustrate-lu
:SCAT: lu
:END:
** Illustrate Merge Sort                                               :task:
:PROPERTIES:
:CUSTOM_ID: mergesort-illustrate-task
:SCAT: task
:END:

*** Merge Sort on an Array                                             :txta:
:PROPERTIES:
:CUSTOM_ID: merge-sort-text
:SCAT: txta
:END:
Given an unsorted, split and merge are performed recursively
to produce a sorted array.

*** Show the Merge Sort                                                :reqa:
:PROPERTIES:
:CUSTOM_ID: merge-sort-show
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an interactive resource that takes a user input of
integers and produces an output sorted array.

* Perform Merge Sort                                                     :lu:
:PROPERTIES:
:CUSTOM_ID: mergesort-exercise-lu
:SCAT: lu
:END:
** Merge Sort Exercise                                                 :task:
:PROPERTIES:
:CUSTOM_ID: mergesort-exercise-task
:SCAT: task
:END:

*** Sort an array using MergeSort                                      :txta:
:PROPERTIES:
:CUSTOM_ID: mergesort-exercise
:SCAT: txta
:END:
Perform split and merge on the given array to produce a
sorted array.

*** Perform the Merge Sort                                             :reqa:
:PROPERTIES:
:CUSTOM_ID: mergesort-even-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs
the split and merge on an array containing even number of 
integers.

*** Perform the Merge Sort                                             :reqa:
:PROPERTIES:
:CUSTOM_ID: mergesort-odd-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs
the split and merge on an array containing odd number of 
integers.
